/* The MIT License

   Copyright (c) 2011, by Greg White <debauchedsloth@gmail.com>

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/
#include "khash.h"
#include <android/log.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/inotify.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define EVENT_SIZE  		(sizeof(struct inotify_event))
#define EVENT_BUF_LEN     	(1024 * (EVENT_SIZE + 16))

KHASH_MAP_INIT_INT(int, char *);
KHASH_MAP_INIT_STR(str, int);

/* TODO: mount hack hardcoded for GTA3, at the moment; can be easily extended if needed */

#ifdef DEBUG
	#define TRACE sdlog(ANDROID_LOG_DEBUG, "%s %d", __FILE__, __LINE__)
#else
	#define TRACE
#endif

static khash_t(int) *watcher_hash;
static khash_t(str) *exclusion_hash;

static int inotify_fd = 0;

static const char *EXCLUSION_NAME = "/mnt/sdcard/.scan_disabled.txt";

#ifdef FORCE_UID
	static int uid = 0;
	static int gid = 0;
#endif

static struct stat st;
static int wd_exclusion = 0;
const char *log_tag = "sdwatch";

static unsigned int dir_flags = S_IRWXU | S_IRWXG | S_IRWXO;
static unsigned int file_flags = S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH;


void sdlog(int prio, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    __android_log_vprint(prio, log_tag, fmt, ap);
#ifdef DEBUG
    vprintf(fmt, ap);
    puts("");
#endif
    va_end(ap);
}

static void add_watch(const char *p)
{
	int wd = inotify_add_watch(inotify_fd, p, IN_CREATE |  IN_ATTRIB | IN_MOVED_TO);
	if(wd <= 0)
		sdlog(ANDROID_LOG_ERROR, "Cannot add watch for %s (%s)", p, strerror(errno));
	else {
#ifdef DEBUG
		sdlog(ANDROID_LOG_DEBUG, "Watch %s successful", p);
#endif
		int is_missing;
		khiter_t k = kh_put(int, watcher_hash, wd, &is_missing);
		kh_value(watcher_hash, k) = strdup(p);
	}
}

static void check_status(struct stat *pst, const char *filename)
{
	unsigned int flags;

#ifdef DEBUG
	sdlog(ANDROID_LOG_DEBUG, "Checking %s", filename);
#endif
	if(pst == NULL) {
		if(stat(filename, &st)) {
			sdlog(ANDROID_LOG_ERROR, "Could not stat %s (%s)", filename, strerror(errno));
			return;
		}
		pst = &st;
	}
	if(S_ISDIR(pst->st_mode))
		flags = dir_flags;
	else
		flags = file_flags;
	if((pst->st_mode & flags) != flags) {
#ifdef DEBUG
		sdlog(ANDROID_LOG_DEBUG, "Fixing perms for %s", filename);
#endif
		chmod(filename, flags);
	}
#ifdef FORCE_UID
	if(uid && (pst->st_uid != uid || pst->st_gid != gid)) {
#ifdef DEBUG
		sdlog(ANDROID_LOG_DEBUG, "Fixing ownership for %s", filename);
#endif
		chown(filename, uid, gid);
	}
#endif
}



static void scan(DIR *dir, char *dirname)
{
	DIR *child;
	char filename[PATH_MAX+1];
	struct dirent *ent;
	char *p;
	int i;

	while((ent = readdir(dir)) != NULL) {
		/* If this is the . or .. directory, take a pass */
		if(!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
			continue;
		if(lstat(ent->d_name, &st)) {
			sdlog(ANDROID_LOG_ERROR, "Could not stat %s (%s)", ent->d_name, strerror(errno));
			continue;
		}
		/* If it's a link, we're not going to touch it it */
		if(S_ISLNK(st.st_mode))
			continue;
		check_status(&st, ent->d_name);
		/* If it's not a directory, we're not going to watch it */
		if(!S_ISDIR(st.st_mode))
			continue;
		child = opendir(ent->d_name);
		if(!child) {
			sdlog(ANDROID_LOG_ERROR, "Could not open directory %s (%s)", ent->d_name, strerror(errno));
			continue;
		}
		/* Build the fully qualified name */
		strcpy(filename, dirname);
		strcat(filename, "/");
		strcat(filename, ent->d_name);
		int excluded = 0;
		/* See if it is excluded */
		khiter_t iter = kh_get(str, exclusion_hash, filename);
	    if(iter == kh_end(exclusion_hash)) {
			chdir(ent->d_name);
			{
				scan(child, filename);
			}
			chdir("..");
			add_watch(filename);
		}
	}
	if(closedir(dir))
		sdlog(ANDROID_LOG_ERROR, "Unable to close directory (%s)", strerror(errno));
}

long ticks()
{
	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC, &tp);
	return(tp.tv_sec * 1000 + tp.tv_nsec / 1000000);
}

static void scan_directories(char *root)
{
	int i;
	static char buffer[PATH_MAX+1];

	/* Clear any existing watches */
	khiter_t k;
    for (k = kh_begin(watcher_hash); k != kh_end(watcher_hash); ++k) {
    	if(!kh_exist(watcher_hash, k))
    		continue;
		inotify_rm_watch(inotify_fd, kh_key(watcher_hash, k));
        free(kh_value(watcher_hash, k));
    }
	kh_clear(int, watcher_hash);

	/* Clear any exclusions */
    for (k = kh_begin(exclusion_hash); k != kh_end(exclusion_hash); ++k) {
    	if(!kh_exist(exclusion_hash, k))
    		continue;
        free((char *) kh_key(exclusion_hash, k));
    }
	kh_clear(str, exclusion_hash);
    /*
     * Read exclusions.  Exclusions are portions of the directory tree which we will not watch.
     */
	FILE *fp = fopen(EXCLUSION_NAME, "r");
	if(fp) {
		while(fgets(buffer, sizeof(buffer), fp)) {
			/* Strip out any line termination characters as well as trailing '/' */
			char *p = strchr(buffer, '\r');
			if(p)
				*p = 0;
			p = strchr(buffer, '\n');
			if(p)
				*p = 0;
			if(!buffer[0])
				continue;
			p = &buffer[strlen(buffer) - 1];
			if(*p == '/')
				*p = 0;
			if(buffer[0]) {
				int is_missing;
				k = kh_put(str, exclusion_hash, strdup(buffer), &is_missing);
				kh_value(exclusion_hash, k) = i+1;
				sdlog(ANDROID_LOG_INFO, "Excluding %s from watch", buffer);
			}
		}
		fclose(fp);
	}
	if(wd_exclusion != 0)
		inotify_rm_watch(inotify_fd, wd_exclusion);
	if(inotify_fd > 0)
		close(inotify_fd);
	inotify_fd = inotify_init();
	if(inotify_fd < 0) {
		sdlog(ANDROID_LOG_ERROR, "inotify_init failed (%s)", strerror(errno));
		exit(-1);;
	}
	/* We watch the exclusion file in its own way, since we care about changes to its contents */
	wd_exclusion = inotify_add_watch(inotify_fd, EXCLUSION_NAME, IN_CREATE | IN_MODIFY | IN_ATTRIB | IN_MOVED_TO | IN_MOVED_FROM | IN_DELETE | IN_CLOSE_WRITE);

	add_watch(root);
	long start = ticks();
	scan(opendir(root), root);
	sdlog(ANDROID_LOG_INFO, "Added %d watches in %dms", kh_size(watcher_hash), ticks() - start);
}

static const char *gta_image_name = "/mnt/sdcard/.sd_images/com.rockstar.gta3.img";

static void mount_images()
{
	DIR *dir;
	struct dirent *ent;
	char filename[PATH_MAX+1];
	char cmd[PATH_MAX+1];
	char mnt[PATH_MAX+1];

	dir = opendir("/mnt/sdcard/.sd_images");
	if(!dir)
		return;
	while((ent = readdir(dir)) != NULL) {
		sprintf(filename, "/mnt/sdcard/.sd_images/%s", ent->d_name);
		if(lstat(filename, &st)) {
			sdlog(ANDROID_LOG_ERROR, "mount_images(): cannot stat %s (%s)", filename, strerror(errno));
			continue;
		}
		if(S_ISDIR(st.st_mode))
			continue;
		sdlog(ANDROID_LOG_INFO, "Trying to mount /mnt/sdcard/.sd_images/%s", ent->d_name);
		/* Setup the target mount point, which is the image name with .img trimmed off */
		sprintf(mnt, "/mnt/sdcard/Android/data/%s", ent->d_name);
		/* Start at the end of the file name and walk backwards looking for a '.', then trim everything from the '.' forward. */
		char *p = mnt + strlen(mnt) - 1;
		while(p >= mnt && *p != '.')
			p--;
		*p = 0;
		sprintf(cmd, "/system/xbin/busybox umount %s", mnt);
		system(cmd);
		sprintf(cmd, "/system/xbin/busybox mount -o loop,rw,dirsync,nosuid,nodev,noexec,noatime,uid=1000,gid=1015,fmask=0702 /mnt/sdcard/.sd_images/%s %s", ent->d_name, mnt);
		system(cmd);
	}
	closedir(dir);
}

static int media_watcher()
{
	int length, i = 0;
	static char filename[FILENAME_MAX+1];
	static char buffer[EVENT_BUF_LEN];

	long start = ticks();
	while( umount2("/mnt/sdcard", MNT_DETACH) ) {
		if(errno == EINVAL) /* EINVAL means it wasn't mounted to start with...that's fine. */
			break;
		sdlog(ANDROID_LOG_INFO, "Unmount failed (%s)...waiting...", strerror(errno));
		if((ticks() - start) > 8000) {
			sdlog(ANDROID_LOG_INFO, "Unmount timed out...forcing unmount");
			if(umount2("/mnt/sdcard", MNT_FORCE))
				sdlog(ANDROID_LOG_INFO, "Forced unmount failed (%s)", strerror(errno));
			break;
		}
		sleep(1);
	}
	if(mount("/data/media", "/mnt/sdcard", "", MS_BIND | MS_NOATIME, "")) {
		sdlog(ANDROID_LOG_ERROR, "Cannot mount sdcard (%s)", strerror(errno));
		exit(-1);
	}
	chmod("/data/media", S_IRWXU | S_IRWXG | S_IRWXO);
	if(chdir("/mnt/sdcard")) {
		sdlog(ANDROID_LOG_ERROR, "Cannot chdir to sdcard (%s)", strerror(errno));
		exit(-1);
	}
	int scan_pending = 1;
	for(;;) {
		if(scan_pending) {
			scan_directories("/mnt/sdcard");
			mount_images();
			scan_pending = 0;
		}
		length = read(inotify_fd, buffer, sizeof(buffer));
		if(length < 0) {
			sdlog(ANDROID_LOG_ERROR, "Error %d reading event buffer (%s)", errno, strerror(errno));
			if(errno != EINTR)
				exit(0);
		}
		i = 0;
		while(i < length) {
			struct inotify_event *event = (struct inotify_event *) &buffer[i];
			if(event->wd == wd_exclusion)
				scan_pending = 1;
			khiter_t iter = kh_get(int, watcher_hash, event->wd);
			if(iter == kh_end(watcher_hash))
				sdlog(ANDROID_LOG_ERROR, "Unable to find watch for %s", event->name);
			else {
				strcpy(filename, kh_value(watcher_hash, iter));
				strcat(filename, "/");
				strcat(filename, event->name);
				if(!strcmp(filename, EXCLUSION_NAME))
					scan_pending = 1;
				int is_dir = (event->mask & IN_ISDIR);
				check_status(NULL, filename);
				if(is_dir) {
					if(event->mask & IN_CREATE) {
						/* If this is GTA3, create a sparse image file for its data and mount it into this directory */
						if(!strcmp(filename, "/mnt/sdcard/Android/data/com.rockstar.gta3")) {
							sdlog(ANDROID_LOG_INFO, "Detected GTA3 install...checking image.");
							int fd =  open(gta_image_name, O_RDONLY);
							if(fd < 0) {
								mkdir("/mnt/sdcard/.sd_images", dir_flags);
								sdlog(ANDROID_LOG_INFO, "Detected GTA3 install, image not found, creating sparse image");
								fd = open(gta_image_name, O_WRONLY | O_CREAT);
								if(fd < 0)
									sdlog(ANDROID_LOG_INFO, "Error creating GTA3 image (%s)", strerror(errno));
								else {
									/* Create a 640MB sparse image to store all the GTA3 data files. */
									lseek(fd, (640*1024*1024)-1, SEEK_SET);
									write(fd, "a", 1);
									close(fd);
									system("mkfs.vfat /mnt/sdcard/.sd_images/com.rockstar.gta3.img");
									mount_images();
								}
							}
							else {
								close(fd);
								sdlog(ANDROID_LOG_INFO, "Detected GTA3 install, image found, trying to mount it");
								mount_images();
							}
						}
						add_watch(filename);
					}
				}
			}
			i += (EVENT_SIZE + event->len);
		}
	}
	close(inotify_fd);
	return 0;
}

int main()
{
	sdlog(ANDROID_LOG_INFO, "Starting (%s %s)", __DATE__, __TIME__);
#ifdef FORCE_UID
	struct passwd *pwbuf = getpwnam("media_rw");
	if(pwbuf) {
		uid = pwbuf->pw_uid;
		gid = pwbuf->pw_gid;
	}
	else
		sdlog(ANDROID_LOG_ERROR, "Cannot get uid/gid (%s)", strerror(errno));
#endif
	watcher_hash = kh_init(int);
	exclusion_hash = kh_init(str);
	media_watcher();
	return 0;
}

